/**
    
 */
function updateTrafficMap() {
    var latitude = trafficViewingAreaLat;
    var longitude = trafficViewingAreaLon;
    var zoom_level = 14;
    var grid_radius = 1;  // 3 x 3
    
    var origin_grid = (calculateLocation(latitude, longitude, zoom_level)).split('/');
    var origin_grid_x = Number(origin_grid[0]);
    var origin_grid_y = Number(origin_grid[1]);
    
    var toHTML = '<div style="overflow:scroll; height: 500px; width: 100%;">';
    for(var i = origin_grid_y-grid_radius; i <= origin_grid_y+grid_radius; i++) {
        for(var j = origin_grid_x-grid_radius; j <= origin_grid_x+grid_radius; j++) {
            console.log('Grid: ' + i + '-' + j);
            toHTML += '<img src="https://1.traffic.maps.api.here.com/maptile/2.1/traffictile/newest/normal.day/14/' + j + '/' + i + '/512/png?app_id=EBzKc5XT9oEOOnDoJqXq&app_code=YJ3g9zTH1QWqo_u_XFh1bw"/>';
            console.log(toHTML);
        }
        toHTML += '<br>';
    }
    toHTML += '</div>';
    document.getElementById('fs-pg-traffic-content').innerHTML = toHTML;
    if(document.getElementById('fs-pg-traffic-content').innerHTML == '') {
        document.getElementById('fs-pg-traffic-content').innerHTML = 'Traffic Data Unavailable';
    }
}

/**
    Mercator projection calculator
 */
function calculateLocation(latitude, longitude, zoom) {
    var latRad = latitude * (Math.PI / 180);
    var n = Math.pow(2, zoom);
    var xTile = n * ((longitude + 180) / 360);
    var yTile = n * (1-(Math.log(Math.tan(latRad) + 1/Math.cos(latRad)) /Math.PI)) / 2;
    console.log('Calculated location column: ' + Math.round(xTile) + ' row: ' + Math.round(yTile));
    return Math.round(xTile) + '/' + Math.round(yTile);
}

