/**
	780Development
	Giakhanh Hoang
	
	home-dashboard/js/newsInfo2.js
	
	This file contains functions for handling data retreived from the News API.
	
	Third party APIs involved:
	- News API
 */
 
 /**
	Retreives a list of news headlines and brief descriptions, along with their respective cover 
	  images if applicable.
	
	Most of the network operations behind this function is based off the template given in the
	  News API documentation for getting started.
	  https://newsapi.org/docs/get-started
	  
	This function makes an API call based on the given URL.
	This function is asynchronous as it is a network operation.
	
	**There is no error handling when there should be one.
	**Consider expanding the function to allow parameters for specific headline requests.
  */

function getNewsInfo() {
    // Custominize the URL according to the news api documentation to recieve more specific 
	//   headlines
    var url = 'https://newsapi.org/v2/top-headlines?' +
          'country=us&' +
          'apiKey=7b7046bf6cae4fceb41823ae5adce32c';
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		// On successful request
        if(this.readyState == 4 && this.status == 200) {
			// Data from "this.responseText" can only be retreived once, so it is stored into a
			//   a variable so that data can continue to be used and manipulated.
			// In this case, the data received is a JSON format that can be converted.
            var jsonObj = JSON.parse(this.responseText);
			// **Should be commented out in production 
            console.log('News retrieval status: ' + jsonObj.status);
            if(jsonObj.status == 'ok') {
                formatNewsInfo(jsonObj);
            }
        }
    }
	// GET request
    xmlhttp.open('GET', url, true);
    xmlhttp.send();
}

function formatNewsInfo(responseObj) {
    var tbodyHtml = '';
    for(var i = 0; i < responseObj.articles.length; i++) {
        var articleImageHTML = '';
        var articleDescription = '';
        // If there is an image, include an element for such, otherwise the element will not be generated for it,
        //  leaving the card with just a headline and such.
        if(responseObj.articles[i].urlToImage != null) {
            articleImageHTML = '<img class=\'card-img-top\' src=\'' + responseObj.articles[i].urlToImage + '\' alt=\'Headline Image\' style=\'max-height:256px\'>';
        }
        // If there is a description, it will be on the card, otherwise there's nothing more
        if(responseObj.articles[i].description != null) {
            articleDescription = responseObj.articles[i].description;
        }
        tbodyHtml += 
            // Container column
            '<div class=\'col-md-3\'>' +
            // Card class, also a container in a sense
            '<div class=\'card news-card\'>' +
            // Image element insertion
            articleImageHTML +
            // Card body element
            '<div class=\'card-body\'>' +
            // Card article title element
            '<h5 class=\'card-title\'>' + responseObj.articles[i].title + '</h5>' +
            // Card article source element
            '<h6 class=\'card-subtitle text-muted\'>' + responseObj.articles[i].source.name + '</h6>' +
            // Card article description element
            '<p class=\'card-text\' style=\'color:black\'>' + articleDescription + '</p>' + 
            // End tags for the first three of the divider elements
            '</div></div></div>';
    }
    // Update element to hold all the retreived and formatted data
    document.getElementById('fs-pg-news-content').innerHTML = tbodyHtml;
}