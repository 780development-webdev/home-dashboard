/**
	780Development
	Giakhanh Hoang
	
	home-dashboard/js/calendarEvents.js
	
	This file contains functions that interact with 780Calendars to retreive events and format them
	  for display on the dashboard.
 */
 
 /**
	Given a full calendar id (fcid), the list of events is retreived and formatted for display in 
	  "home-dashboard/events.html".
	
	This function makes a network call to "/app/calendar/sv/getEventsPublic.php"
	This function is asynchronous as it is a network operation.
  */
function getCalendarEventsForID(fcid) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		// On successful request
        if(this.readyState == 4 && this.status == 200) {
			// Data from "this.responseText" can only be retreived once, so it is stored into a
			//   a variable so that data can continue to be used and manipulated.
            var responseText = this.responseText;
			// Split returned data by newline characters
            var responseArr = responseText.split('\n');
            var eventListHTML = '';
            for(var i = 0; i < responseArr.length-1; i = i+8) {
				// **Should be commented out in production 
                console.log('Event ID: ' + responseArr[i] + 
                            '\nUser: ' + responseArr[i+1] + 
                            '\nDate Start: ' + responseArr[i+2] + 
                            '\nDate End: ' + responseArr[i+3] + 
                            '\nEvent Name: ' + responseArr[i+4] + 
                            '\nEvent Description: ' + responseArr[i+5] + 
                            '\nPriority: ' + responseArr[i+6]);
				// Should use a utility function from /wslib/util/util.js to convert from a MYSQL
				//   timestamp to a Javascript timestamp
                var begDateParts = responseArr[i+2].split(/[- :]/);
                var endDateParts = responseArr[i+3].split(/[- :]/);
                var dateStart = new Date(begDateParts[0],       // Year
                                         begDateParts[1] - 1,   // Month
                                         begDateParts[2],       // Day
                                         begDateParts[3],       // Hour
                                         begDateParts[4],       // Minute
                                         begDateParts[5]);      // Second
                var dateEnd = new Date(endDateParts[0],
                                       endDateParts[1] - 1, 
                                       endDateParts[2],
                                       endDateParts[3],
                                       endDateParts[4],
                                       endDateParts[5]);
				// Only display events that have not occured yet
                var today = new Date();
                if(today.getTime() < dateEnd.getTime()) {
					// Start of card class information, treated as a container
                    eventListHTML += '<div class=\'card\' style=\'width: 90%; margin-top: 12px; background-color: rgba(0, 0, 0, 0.4)\'>' +
										// A row that is a part of the card-body
                                        '<div class=\'card-body row\'>' +
										// An image would go in the the div below, but for now the color is instead set based on priority number
                                        '<div class=\'col-md-1\' style=\'background-color:' + determinePriorityColor(responseArr[i+6]) + '; border-radius:5px\'></div>' +
										// Another subcomplex of card formats
                                        '<div class=\'col-md-11\'>' +
                                        '<div class=\'card-block\' style=\'color:white\'>' +
                                        '<div class=\'row\'>' +
                                        '<div class=\'col-md-6\'>' +
										// Event name 
                                        '<h5 class=\'card-title\' style=\'font-size: 36px\'>' + responseArr[i+4] + '</h5>' +
                                        '</div>' +
										// Event date range
                                        '<div class=\'col-md-6\' style=\'text-align: right\'>' +
                                        '<h5 class=\'card-subtitle\' style=\'font-size: 36px\'>' + formatDateRange(dateStart, dateEnd) + '</h5>' +
                                        '</div></div>' +
										// Event description
                                        '<p class=\'card-text\' style=\'font-size: 28px\'>' + responseArr[i+5] + '</p>' +
                                        '</div></div></div></div>';
                }
            }
			// Update element to hold all the events
            document.getElementById('events_list').innerHTML = eventListHTML;
            // If the element is still empty (no events), then insert the empty events message
            if(document.getElementById('events_list').innerHTML == '') {
                document.getElementById('events_list').innerHTML = 
                    '<div class=\'card cal-events-card\'>' +
                        '<div class=\'card-body row\'>' +
                            '<div class=\'col-md-2 text-center\'>' +
                                '<img class=\'cal-event-img\' src=\'icons/calendar.png\'>' +
                            '</div>' +
                            '<div class=\'col-md-10\'>' +
                                '<div class=\'card-block cal-events-text\' style=\'color:white\'>' +
                                    '<div class=\'row\'>' +
                                        '<div class=\'col-md-6 cal-events-title\'> No upcoming events </div>' +
                                        '<div class=\'col-md-6 cal-events-date\'></div>' +
                                        '</div> <p class=\'cal-events-descrp\'>You can add events from the calendar service or change calendar displayed in settings.</p> </div>' +
                                        '<hr style=\'background-color:white\'>' + 
                                    '</div>' +
                                '</div>' + 
                            '</div>' +
                        '</div>'+
                    '</div>';
            }
        } 
		// In the event the network operation fails
		else {
            console.log(this.responseText);
        }
    }
	// POST request, passing in calendar id
    xmlhttp.open('POST', '/app/calendar/sv/getEventsPublic.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('app_full_calendar_id=' + fcid);
}

/**
	Takes a number (a priority number) and returns a related color code. If an unknown number is 
	  specified, the default is the color "grey".
	
	Current color code:
	  1 - [Urgent] 				- Red
	  2 - [Important] 			- Yellow
	  3 - [Normal] 				- Green
	  4 - [Information Only] 	- Blue
 */
function determinePriorityColor(num) {
    switch(num) {
        case '1':
            return 'red';
            break;
        case '2':
            return 'yellow';
            break;
        case '3':
            return 'green';
            break;
        case '4':
            return 'blue';
            break;
        default:
            return 'grey';
            break;
    }
}