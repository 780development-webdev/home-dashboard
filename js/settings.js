var CORS_LINK_MAIN = 'https://cors-anywhere.herokuapp.com/';
var CORS_LINK_BKUP = 'https://cors.io/?';

var loggedIn = false;
var username = '';

// Weather information 
var weatherCity = 'Dallas,TX';          // Set by cookie 'home_dash_weather_city'

// Traffic information
var trafficCity = 'Dallas,TX';          // Set by cookie 'home_dash_traffic_city'
var trafficZip = '75252';               // Set by cookie 'home_dash_traffic_zip'
var trafficViewingAreaLat = 32.997609;
var trafficViewingAreaLon = -96.767837;

// Calendar information
var calendarName = 'None'               // Set by cookie 'home_dash_cid'
var calendarCid = '';                   // Set by cookie 'home_dash_calendar_name'
var calendarNamesList = [];

function initializeSettingsLegacy() {
    
}

function validateLibraryInclusion() {
    if(!(typeof authenticateToken === "function")) {
        console.log('/wslib/auth/auth.js does not exist since authenticateToken is not available');
    }
    if(!(typeof getCookie === "function")) {
        console.log('/wslib/util/util.js does not exist since getCookie is not available');
    }
    
    return typeof authenticateToken === "function" && typeof getCookie === "function";
}

function initializeSettings() {
    authenticateToken();
    
    // Load saved weather information
    var savedWeatherCity = getCookie('home_dash_weather_city');
    if(savedWeatherCity != null && savedWeatherCity != '')
        weatherCity = savedWeatherCity;
    
    // Load saved traffic information
    var savedTrafficCity = getCookie('home_dash_traffic_city');
    if(savedTrafficCity != null && savedTrafficCity != '') 
        trafficCity = savedTrafficCity;
    
    var savedTrafficZip = getCookie('home_dash_traffic_zip');
    if(savedTrafficZip != null && savedTrafficZip != '')
        trafficZip = savedTrafficZip;
    
    // Load saved calendar information
    var savedCid = getCookie('home_dash_cid');
    if(savedCid != null && savedCid != '')
        calendarCid = savedCid;
    
    var savedCalendarName = getCookie('home_dash_calendar_name');
    if(savedCalendarName != null && savedCalendarName != '')
        calendarName = savedCalendarName;
    
    // Load elements to be used in initialization
    var element_cal_name = document.getElementById('option_calendar_name');
    var element_dash_legacy_messasge = document.getElementById('message');
    var element_weather_location = document.getElementById('option_weather_location');
    var element_traffic_location = document.getElementById('option_traffic_city');
    var element_traffic_zipcode = document.getElementById('option_traffic_zipcode');
    
    // Set calendar information placeholder
    if(element_weather_location) {
        element_weather_location.placeholder = weatherCity;
    }
    // Set traffic information placeholder
    if(element_traffic_location)
        element_traffic_location.placeholder = trafficCity;
    if(element_traffic_zipcode)
        element_traffic_zipcode.placeholder = trafficZip;
    
    // Set calendar information placeholder
    if(element_cal_name)
        element_cal_name.innerHTML = 'Events displayed from calendar: <br> ' + calendarName;
    
    // LEGACY ONLY: Set calendar message
    if(calendarCid != null && calendarCid != '' && element_dash_legacy_messasge)
        element_dash_legacy_messasge.innerHTML = 'Currently using calendar ID: ' + calendarCid;
    else if(element_dash_legacy_messasge)
        element_dash_legacy_messasge.innerHTML = '';
    
    // Set HTML information
    document.getElementById('fs-pg-forecast-location').innerHTML = weatherCity + '<br> Powered by Here API - Weather';
    document.getElementById('foreground_sub_info_bar_1').innerHTML = weatherCity;
    document.getElementById('fs-pg-traffic-location').innerHTML = trafficCity + ' ' + trafficZip + '<br> Powered by Here API - Weather';
}

function gotologin() {
    document.cookie = 'loginRedirect=home-dashboard/index2.html; path=/';
    document.location = '/';
}

function logout() {
    window.location = '/logout.html';
}

function saveChanges() {
    setWeatherLocation();
    setTrafficViewingLocation();
    setCalendarIDForDash();
}

function onAuthPass(response) {
    console.log('Authentication Successful');
    loggedIn = true;
    username = getCookie('web_username');
    
    // Load elements to be used
    var element_login_box = document.getElementById('option_login_box');
    var element_weather_location = document.getElementById('option_weather_location');
    var element_traffic_location = document.getElementById('option_traffic_city');
    var element_traffic_zipcode = document.getElementById('option_traffic_zipcode');
    var element_cal_select = document.getElementById('cal_id_select');
    var element_submit_change = document.getElementById('option_submit_changes');
    
    // Adjust settings view so that the authorized user can make changes and/or logout
    if(element_login_box)
        element_login_box.innerHTML = 
            '<p>Logged in as: ' + username + '</p>' +
            '<button id=\'login_btn\' class=\'btn btn-danger\' onclick=\'logout()\'>Logout</button>';
    if(element_weather_location)
        element_weather_location.disabled = false;
    if(element_traffic_location)
        element_traffic_location.disabled = false;
    if(element_traffic_zipcode)
        element_traffic_zipcode.disabled = false;
    if(element_cal_select)
        element_cal_select.disabled = false;
    if(element_submit_change)
        element_submit_change.classList.remove('disabled');
    
    retrieveCalendars();
    initializeDashInformation();
}

function onAuthFail(response) {
    // Load elements to be used
    var element_dash_legacy_message = document.getElementById('message');
    var element_option_message = document.getElementById('option_message');
    
    // Set message
    if(element_option_message)
        element_option_message.innerHTML = 'You must be logged in to make changes';
    // LEGACY ONLY: Set message
    if(element_dash_legacy_message)
        element_dash_legacy_message.innerHTML = 'You must be logged in to make changes';
    
    initializeDashInformation();
}

function initializeDashInformation() {
    getLocalTime();
    getCurrentWeatherInfo(CORS_LINK_MAIN);
    getWeatherForecast(CORS_LINK_MAIN);
    getNewsInfo();
    setGeocodeLocation(trafficCity, trafficZip);

    // Set repeating intervals
    setInterval(function() {getCurrentWeatherInfo(CORS_LINK_MAIN)}, 900000);        //            ...15 minutes
    setInterval(function() {getWeatherForecast(CORS_LINK_MAIN)}, 900000);           //            ...15 minutes
    setInterval(function() {getNewsInfo()}, 900000);                                //            ...15 minutes (minimum possible with News API)
    setInterval(function() {updateTrafficMap()}, 900000);                           //            ...15 minutes
    
    if(calendarCid != null && calendarCid != '') {
        getCalendarEventsForID(calendarCid);
        setInterval(function() {getCalendarEventsForID(calendarCid)}, 900000);      //            ...15 minutes
    }
}

function setWeatherLocation() {
    var element_weather_location_input = document.getElementById('option_weather_location');
    if(element_weather_location_input.value != null && element_weather_location_input.value != '') {
        weatherCity = document.getElementById('option_weather_location').value;
    
        // Set never expiring cookies
        var today = new Date();
        today.setTime(today.getTime() + 1000*2592000);
        document.cookie = 'home_dash_weather_city=' + weatherCity /*+ '; expires=' + today.toUTCString()*/;

        console.log('Now using weather location ' + weatherCity);
        
        // Update HTML information
        document.getElementById('fs-pg-forecast-location').innerHTML = weatherCity + '<br> Powered by Here API - Weather';
        document.getElementById('foreground_sub_info_bar_1').innerHTML = weatherCity;
        
        // Update weather information
        getCurrentWeatherInfo(CORS_LINK_MAIN);
        getWeatherForecast(CORS_LINK_MAIN);
    }
}

function setTrafficViewingLocation() {
    var element_traffic_city_input = document.getElementById('option_traffic_city');
    var element_traffic_zip_input = document.getElementById('option_traffic_zipcode');
    // Only make changes if values are not empty and or the values are the same as the current
    if(element_traffic_city_input.value != null && element_traffic_city_input.value != '' 
       && element_traffic_zip_input.value != null && element_traffic_zip_input.value != ''
       && element_traffic_city_input.value != trafficCity && element_traffic_zip_input != trafficZip) {
        // Set new traffic viewing information
        trafficCity = element_traffic_city_input.value;
        trafficZip = element_traffic_zip_input.value;
        
        // Update HTML information
        document.getElementById('fs-pg-traffic-location').innerHTML = trafficCity + ' ' + trafficZip + '<br> Powered by Here API - Weather';
        
        // Set never expiring cookies
        //var today = new Date();
        //today.setTime(today.getTime() + 1000*2592000);
        document.cookie = 'home_dash_traffic_city=' + trafficCity /*+ '; expires=' + today.toUTCString()*/;
        document.cookie = 'home_dash_traffic_zip=' + trafficZip /*+ '; expires=' + today.toUTCString()*/;

        setGeocodeLocation(trafficCity, trafficZip);
    }
}

function setGeocodeLocation(city, zip) {
    // Retreive new coordinates for traffic viewing
    var url = 'https://geocoder.api.here.com/6.2/geocode.json?' + 
        'app_id=EBzKc5XT9oEOOnDoJqXq&app_code=YJ3g9zTH1QWqo_u_XFh1bw&' + 
        'searchtext=' + city + '-' + zip;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        // On successful request
        if(this.readyState == 4 && this.status == 200) {
            // Data from "this.responseText" can only be retreived once, so it is stored into a
            //   a variable so that data can continue to be used and manipulated.
            // In this case, the data received is a JSON format that can be converted.
            var jsonObj = JSON.parse(this.responseText);
            console.log(jsonObj);
            trafficViewingAreaLat = jsonObj.Response.View[0].Result[0].Location.DisplayPosition.Latitude;
            trafficViewingAreaLon = jsonObj.Response.View[0].Result[0].Location.DisplayPosition.Longitude;
            // Update traffic map
            console.log('Now using map lat: ' + trafficViewingAreaLat + ', lon: ' + trafficViewingAreaLon);
            updateTrafficMap();
        }
    }
    xmlhttp.open('GET', url, true);
    xmlhttp.send();
}

function retrieveCalendars() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            // Get formatted calendar list from the server
            var responseText = this.responseText;
            var responseArr = responseText.split('\n');
            var calendarListHTML = '';
            //calendarNamesList = 
            // Parse the formatted list
            for(var i = 0; i < responseArr.length-1; i = i+7) {
                console.log('ID: ' + responseArr[i] + '\nName: ' + responseArr[i+2] + '\nAuthorized: ' + responseArr[i+3] + '\nNumber of Events: ' + responseArr[i+4]);
                // Format HTML form option value
                calendarListHTML += '<option value=' + responseArr[i+5] + '>' + 'ID ' + responseArr[i+5] + ': ' + responseArr[i+2] + '</>';
                // Add name to a running list of calendar names
                calendarNamesList[responseArr[i+5]] = responseArr[i+2];
            }
            // Update HTML with calendar list
            document.getElementById('cal_id_select').innerHTML = calendarListHTML;
        }
    }
    xmlhttp.open('POST', '/app/calendar/sv/getCalendars.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send();
}

function setCalendarIDForDash() {
    var element_dash_legacy_messasge = document.getElementById('message');
    var element_cal_name = document.getElementById('option_calendar_name');
    
    if(loggedIn) {
        // Get calendar ID value and the corresponding calendar name
        calendarCid = document.getElementById('cal_id_select').value;
        calendarName = calendarNamesList[calendarCid];
        
        // Set cookies
        //var today = new Date();
        //today.setTime(today.getTime() + 1000*2592000);
        document.cookie='home_dash_cid=' + calendarCid /*+ '; expires=' + today.toUTCString()*/;
        document.cookie='home_dash_calendar_name=' + calendarName /*+ '; expires=' + today.toUTCString()*/;
        
        // Update HTML info for legacy dashboard options page
        if(element_dash_legacy_messasge)
            element_dash_legacy_messasge.innerHTML = 'Changes confirmed! Now using ID ' 
                + document.getElementById('cal_id_select').value + ' for the events slide';
        // Update HTML info on settings tab
        else element_cal_name.innerHTML = 'Events displayed from calendar: <br> ' + calendarName;
        
        console.log('Now using ID ' + document.getElementById('cal_id_select') 
                    + ' with cal name ' + calendarNamesList[document.getElementById('cal_id_select').value]);
        
        // Update calendar events
        getCalendarEventsForID(calendarCid);
        
    } 
    // Update HTML info for legacy dashboard options page
    else if(element_dash_legacy_messasge)
        element_dash_legacy_messasge.innerHTML = 'You must be logged in to make changes';
}