/**
	780Development
	Giakhanh Hoang
	
	home-dashboard/js/localInfo.js
	
	This file contains utility functions (subject to relocation) and 
	functions to determine local information such as date and time.
 */

var catBase = [
    '0.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '1.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '2.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '3.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '4.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '5.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '6.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '7.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '8.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '9.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)'
];

var catNight = [
    '0.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '1.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '2.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '3.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '4.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '5.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '6.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '7.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '8.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)',
    '9.png|(0, 0, 0, 0.2)|(0, 0, 0, 0.0)|(0, 0, 0, 0.8)'
];

var generalBackgroundCat = [
    catBase, catNight
];
var generalBackgroundPath = 'bgs/g/minecraft/';
var catNightPath = 'nt/';

 /**
	Updates the specified elements on the homepage with the current time.
	To be used only in "home-dashboard/index.html". 
  */
function getLocalTime() {
    document.getElementById('foreground_time').innerHTML = formatAMPM(new Date());
    document.getElementById('fullscreen_time').innerHTML = formatAMPM(new Date());
    document.getElementById('foreground_greeting').innerHTML = formatGreeting(new Date());
    document.getElementById('fullscreen_greeting').innerHTML = formatGreeting(new Date());
    document.getElementById('foreground_info_bar_greeting').innerHTML = getTimeOfDayGreeting(new Date());
}

/**
	Updates the specified element id with the local time.
 */
function getLocalTimeManual(id) {
    document.getElementById(id).innerHTML = formatAMPM(new Date());
}

/**
	Takes a date object and returns a formatted time string in the format in AM/PM:
	  12:00 AM (example only)
 */
function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function getTimeOfDayGreeting(date) {
    var hours = date.getHours();
    var greeting = hours >= 21 ? 'Good Night' : hours >= 17 ? 'Good Evening' : hours > 12 ? 'Good Afternoon' : hours == 12 ? 'Good Noon' : hours >= 5 ? 'Good Morning' : 'Good Night';
    return greeting;
}

function isNight(date) {
  var hours = date.getHours();
  return hours >= 20 || hours < 6;
}
/**
	Takes a date object and returns a string containing a greeting based on the information provided
 */
function formatGreeting(date) {
    var hours = date.getHours();
    var month = date.getMonth();
    var monthStr;
	// Determine the month, should switch to using the function from /wslib/util/util.js
    switch(month) {
        case 0:
            monthStr = 'January';
            break;
        case 1:
            monthStr = 'February';
            break;
        case 2:
            monthStr = 'March';
            break;
        case 3:
            monthStr = 'April';
            break;
        case 4:
            monthStr = 'May';
            break;
        case 5:
            monthStr = 'June';
            break;
        case 6:
            monthStr = 'July';
            break;
        case 7:
            monthStr = 'August';
            break;
        case 8:
            monthStr = 'September';
            break;
        case 9:
            monthStr = 'October';
            break;
        case 10:
            monthStr = 'November';
            break;
        case 11:
            monthStr = 'December';
            break;
        default:
            break;
    }
	// Determine weekday of the week >> Should convert into a utility function for 
	//   /wslib/util/util.js
    var weekDay = date.getDay();
    var weekDayStr;
    switch(weekDay) {
        case 0:
            weekDayStr = 'Sunday';
            break;
        case 1:
            weekDayStr = 'Monday';
            break;
        case 2:
            weekDayStr = 'Tuesday';
            break;
        case 3:
            weekDayStr = 'Wednesday';
            break;
        case 4:
            weekDayStr = 'Thursday';
            break;
        case 5:
            weekDayStr = 'Friday';
            break;
        case 6:
            weekDayStr = 'Saturday';
            break;
        default:
            break;
    }
	// Greeting part may be reimplemented at a later time
    return /*greetingPart1 + '<br> Today is ' + */weekDayStr + ', ' + monthStr + ' ' + date.getDate() + ', ' + date.getFullYear();
}

/** 
    Inactivity function
    Will send the user back to the main page if no activity is detected within 30 minutes
    Original post: https://stackoverflow.com/questions/667555/how-to-detect-idle-time-in-javascript-elegantly
    Specific: https://stackoverflow.com/a/10126042
*/ 
var inactivityActionForIdleOnStatic = function () {
	// Holds the timeout function
    var timer;
	
	// Register actions to take when the following events occur
    window.onload = resetTimer;         // Window loads
    document.onmousemove = resetTimer;  // Mouse cursor movement over browser
    document.onmousedown = resetTimer;  // Touchscreen depress
    document.ontouchstart = resetTimer; // Touchscreen start press
    document.onclick = resetTimer;      // Mouseclicks
    document.onscroll = resetTimer;     // Scrolling with arrow keys
    document.onkeypress = resetTimer;   // Keyboard usage
    
    // Go to homepage
    function finish() {
        window.location = 'index.html'
    }
    
    //Self-explanatory
    function resetTimer() {
        // Clear timer
        clearTimeout(timer);
        // Set 30 minute timeout
        timer = setTimeout(finish, 1800000);
    }
};

function setRandomGeneralBackground() {
    var night = isNight(new Date());
    var backgroundSet = night ? catNight : catBase;
    var finalPath = night ? generalBackgroundPath + catNightPath : generalBackgroundPath;
    
    var backgroundInfo = backgroundSet[Math.floor(Math.random() * backgroundSet.length)].split('|');
    
    document.getElementById('foreground').style.backgroundImage = 
        'linear-gradient(rgba' + backgroundInfo[1] + ' 10%, rgba ' + backgroundInfo[2] + ', rgba' + backgroundInfo[3] + ' 100%)';
    document.getElementById('background').style.backgroundImage = 'url(' + finalPath + backgroundInfo[0] + ')';
}