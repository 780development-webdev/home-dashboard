/**
	780Development
	Giakhanh Hoang
	
	home-dashboard/js/weatherInfo.js
	
	This file contains functions that retreive and interacts with information from Open Weather Map
	  API.
	  
	Third party APIs involved:
	- OpenWeatherMap API
 */
 
 /**
	Requests current weather information including temperature and weather description.
	
	This function makes an API call based on the given URL.
	This function is asynchronous as it is a network operation.
	
	**ONLY WEATHER FOR ZIP CODE 75252 IS RETREIVED, LOOK AT THE URL LINK FOR THAT PARAMETER
  */
function getWeatherInfo() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		// On successful request
        if(this.readyState == 4 && this.status == 200) {
			// Data from "this.responseText" can only be retreived once, so it is stored into a
			//   a variable so that data can continue to be used and manipulated.
			// In this case, the data received is a JSON format that can be converted.
            var obj = JSON.parse(this.responseText);
			// Set weather description
            document.getElementById('weatherDescription').innerHTML = obj.weather[0].main + ', ' + obj.main.temp + '&deg;F';
			// **Old, probably won't be using the default weather icons anymore
            //document.getElementById('weatherIcon').src = 'http://openweathermap.org/img/w/' + obj.weather[0].icon + '.png';
			// Set weather icon
            document.getElementById('weatherIcon').src = 'icons/' + obj.weather[0].icon + '.png';
			// Set background image for the main slide based on the weather
            document.getElementById('frontPageImg').style.backgroundImage = 'url(\'bgs/w/' + obj.weather[0].icon + '.png\')';
        }
    }
    xmlhttp.open('GET', 'http://api.openweathermap.org/data/2.5/weather?zip=75252,us&units=imperial&APPID=1dcb485686c8f428d783ff22a9b317ca', true);
    xmlhttp.send();
}

 /**
	Requests a forecast list of weather information including temperature and weather description
	  for every three hours from the time it is called for the next 5 days.
	
	This function makes an API call based on the given URL.
	This function is asynchronous as it is a network operation.
	
	**ONLY WEATHER FOR ZIP CODE 75252 IS RETREIVED, LOOK AT THE URL LINK FOR THAT PARAMETER
  */
function getWeatherForecast() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		// On successful request
        if(this.readyState == 4 && this.status == 200) {
			// Data from "this.responseText" can only be retreived once, so it is stored into a
			//   a variable so that data can continue to be used and manipulated.
			// In this case, the data received is a JSON format that can be converted.
            var obj = JSON.parse(this.responseText);
            var htmlStr = '';
            for(var i = 0; i < obj.cnt; i++) {
				// Convert datetime given from the API to JS date object
                var curDate = new Date(new Number(obj.list[i].dt) * 1000);
				// Gets a string containing the day of the week and time
                var curDay = determineDay(curDate);
				// Take the highest and lowest predicted temperature from the API and get the 
				//   average
                var avgTemp = ((new Number(obj.list[i].main.temp_max)) + (new Number(obj.list[i].main.temp_min)))/2;
                htmlStr += 
					// Container column
					'<div class=\'col-md-3\'>' +
					// Card class, also a container in a sense
					'<div class=\'card\' style=\'width: 18rem; color:white; background-color: rgba(0, 0, 0, 0.4)\'>' +
					// Card body element
					'<div class=\'card-body\'>' +
					// Card article title element
                    '<h5 class=\'card-title\' style=\'font-size:28px\'>' + curDay + '</h5>' +
					// Card icon/image element
					'<img src=\'icons/' + obj.list[i].weather[0].icon + '.png\'>' +
					// Average temperature
					'<h5 class=\'card-subtitle\' style=\'font-size:64px\'>' + avgTemp.toFixed(0) + '&deg;F </h5>' +
					// Weather description
					'<h6 class=\'card-subtitle\' style=\'font-size:26px; padding-top:12px\'>' + captializeDescription(obj.list[i].weather[0].description) + '</h6>' + 
					// Rain volume and wind information in a paragraph element
					'<p class=\'card-text\' style=\'color:white; font-size:24px\'>' +
					'Rain Volume: ' + determinePrecipitation(obj.list[i].rain) + '<br>' +
					'Wind: ' + determineWind(obj.list[i].wind) +
					'</p></div></div></div>';
            }
			// Update element to hold all the retreived and formatted data
            document.getElementById('weatherForecast').innerHTML = htmlStr;
        }
    }
    xmlhttp.open('GET', 'http://api.openweathermap.org/data/2.5/forecast?zip=75252,us&units=imperial&APPID=1dcb485686c8f428d783ff22a9b317ca', true);
    xmlhttp.send();
}

/**
	Determine weekday of the week along with the time given a date object.
	
	**Should convert into a utility function for "/wslib/util/util.js"
 */
function determineDay(date) {
    var weekDay = date.getDay();
    var weekDayStr;
    switch(weekDay) {
        case 0:
            weekDayStr = 'Sunday';
            break;
        case 1:
            weekDayStr = 'Monday';
            break;
        case 2:
            weekDayStr = 'Tuesday';
            break;
        case 3:
            weekDayStr = 'Wednesday';
            break;
        case 4:
            weekDayStr = 'Thursday';
            break;
        case 5:
            weekDayStr = 'Friday';
            break;
        case 6:
            weekDayStr = 'Saturday';
            break;
        default:
            break;
    }
    var hours = date.getHours();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
	// I don't think this line below actually does anything...
    hours = hours ? hours : 12; // the hour '0' should be '12'
    return weekDayStr + ', ' + hours + ' ' + ampm;
}

/**
	Formats the given precipitation data (a converted JSON object) and returns a formatted string 
	  with units.
 */
function determinePrecipitation(data) {
	// Data from OpenWeatherMap API for this part CAN BE EMPTY OR NULL
	//   because it is not written.
    if(data != null && data['3h'] != null) {
        return data['3h'].toFixed(2) + ' mm';
    } else {
        return '0 mm';
    }
}

/**
	Formats the given wind data (a converted JSON object) and returns a formatted string 
	  with wind speed and meteorological direction.
 */
function determineWind(data) {
	// Data from OpenWeatherMap API for this part CAN BE EMPTY OR NULL
	//   because it is not written.
    if(data != null) {
        var dir = '';
        var dirNum = new Number(data.deg);
        // Source for interpreting meteorological direction
        // http://snowfence.umn.edu/Components/winddirectionanddegreeswithouttable3.htm
        if((dirNum > 348.75 && dirNum < 360) || (dirNum <= 11.25 && dirNum > 0)) {
            dir = 'N';
        } else if (dirNum > 11.25 && dirNum <= 33.75) {
            dir = ', NNE';
        } else if (dirNum > 33.75 && dirNum <= 56.25) {
            dir = ', NE';
        } else if (dirNum > 56.25 && dirNum <= 78.75) {
            dir = ', ENE';
        } else if (dirNum > 78.75 && dirNum <= 101.25) {
            dir = ', E';
        } else if (dirNum > 101.25 && dirNum <= 123.75) {
            dir = ', ESE';
        } else if (dirNum > 123.75 && dirNum <= 146.25) {
            dir = ', SE';
        } else if (dirNum > 146.25 && dirNum <= 168.75) {
            dir = ', SSE';
        } else if (dirNum > 168.75 && dirNum <= 191.25) {
            dir = ', S';
        } else if (dirNum > 191.25 && dirNum <= 213.75) {
            dir = ', SSW';
        } else if (dirNum > 213.75 && dirNum <= 236.25) {
            dir = ', SW';
        } else if (dirNum > 236.25 && dirNum <= 258.75) {
            dir = ', WSW';
        } else if (dirNum > 258.75 && dirNum <= 281.25) {
            dir = ', W';
        } else if (dirNum > 281.25 && dirNum <= 303.75) {
            dir = ', WNW';
        } else if (dirNum > 303.75 && dirNum <= 326.25) {
            dir = ', NW';
        } else if (dirNum > 326.25 && dirNum <= 348.75) {
            dir = ', NNW';
        }
        return data.speed + ' MPH' + dir;
    } else {
        return '0 MPH';
    }
}

/**
	Captializes the the words of a given input string (data). 
	
	**Should relocate to "/wslib/util/util.js"
 */
function captializeDescription(data) {
    if(data != null) {
        var splitArr = data.split(" ");
        var returnString = '';
        for(var i = 0; i < splitArr.length; i++) {
            returnString += splitArr[i].charAt(0).toUpperCase() + splitArr[i].substr(1) + ' ';
        }
        return returnString;
    } else {
        return 'No Data';
    }
}