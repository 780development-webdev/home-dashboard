/**
	780Development
	Giakhanh Hoang
	
	home-dashboard/js/weatherInfo2.js
	
	This file contains functions that retreive and interacts with information from Here API for 
      weather information.
    
    Differences from the old weatherInfo.js:
    - Since the main home-dashboard page is different, there are different id elements that are 
        updated.
    - We no longer need to determine wind direction based on a cardinal number and no longer need to 
        capitalize stuff.
    
	Third party APIs involved:
	- Here API
 */

 /**
	Requests current weather information including temperature and weather description.
	
	This function makes an API call based on the given URL.
	This function is asynchronous as it is a network operation.
	
  */
var EL_WEATHER_FULL_CONTENT = 'fs-pg-forecast-content';
var EL_WEATHER_INFOBAR_CONTENT = 'fg-cb-2-p';
var EL_CURRENT_WEATHER_INFOBAR_CONTENT = 'foreground_sub_info_bar_2';
var EL_CURRENT_WEATHER_ICON_INFOBAR_CONTENT = 'foreground_info_bar_cur_weather_icon';

var getCurrentWeatherInfoCORSBackupUsed = false;
var getWeatherForecastCORSBackupUsed = false;

function getCurrentWeatherInfo(cors_link) {
    // Get location parameter from cookie
    var cityLocation = weatherCity;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		// On successful request
        if(this.readyState == 4 && this.status == 200) {
			// Data from "this.responseText" can only be retreived once, so it is stored into a
			//   a variable so that data can continue to be used and manipulated.
			// In this case, the data received is a JSON format that can be converted.
            formatWeatherObservation(this.responseText);
        } else {
            console.log('getCurrentWeatherInfo request status ' + this.status + ' attempting to switch to CORS backup');
            //if(!getCurrentWeatherInfoCORSBackupUsed) {
            //    getCurrentWeatherInfoCORSBackupUsed = true;
            //    getCurrentWeatherInfo(CORS_LINK_BKUP);
            //}
            //else console.log('CORS backup already used. Nothing is done.');
        }
    }
    /*https://cors.io/?*/
    xmlhttp.open('GET', cors_link + 'https://weather.api.here.com/weather/1.0/report.json?app_id=EBzKc5XT9oEOOnDoJqXq&app_code=YJ3g9zTH1QWqo_u_XFh1bw&product=observation&name=' + cityLocation, true);
    xmlhttp.send();
}

function formatWeatherObservation(responseText) {
    var htmlStr = '';
    var obj = JSON.parse(responseText);
    var firstResult = obj.observations.location[0].observation[0];
    document.getElementById(EL_CURRENT_WEATHER_ICON_INFOBAR_CONTENT).innerHTML = 
        '<img src=\'' + determineWeatherIcon(firstResult.iconLink, firstResult.iconName) + '\' alt=\'Weather Icon\'>' 
    document.getElementById(EL_CURRENT_WEATHER_INFOBAR_CONTENT).innerHTML = Math.round(convertFromCelciusToFarenheit(Number(firstResult.temperature))) + '&deg;F ' + capitalizeDescription(firstResult.description);
}

function determineMainBackground(iconLink) {
    
}
 /**
	Requests a forecast list of weather information including temperature and weather description
	  for the next seven days.
      
	This function makes an API call based on the given URL.
	This function is asynchronous as it is a network operation.
	
  */
function getWeatherForecast(cors_link) {
    // Get location parameter from cookie
    var cityLocation = weatherCity;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		// On successful request
        if(this.readyState == 4 && this.status == 200) {
			// Data from "this.responseText" can only be retreived once, so it is stored into a
			//   a variable so that data can continue to be used and manipulated.
			// In this case, the data received is a JSON format that can be converted and sent 
            //   to the following function to be formatted for display
			var result = this.responseText;
			console.log(result);
            formatWeatherForecast(result);
        } else {
            console.log('getWeatherForecast request status ' + this.status + ' attempting to switch to CORS backup');
            //if(!getWeatherForecastCORSBackupUsed) {
            //    getWeatherForecastCORSBackupUsed = true;
            //    getWeatherForecast(CORS_LINK_BKUP);
            //}
            //else console.log('CORS backup already used. Nothing is done.');
        }
    }
    xmlhttp.open('GET', cors_link + 'https://weather.api.here.com/weather/1.0/report.json?app_id=EBzKc5XT9oEOOnDoJqXq&app_code=YJ3g9zTH1QWqo_u_XFh1bw&product=forecast_7days_simple&name=' + cityLocation, true);
    xmlhttp.send();
}


function formatWeatherForecast(responseText) {
    var obj = JSON.parse(responseText);
    //var obj = JSON.parse(forecastTestCase);
    //console.log(obj.dailyForecasts.forecastLocation.forecast[0]);
    var htmlStr = '';
    var htmlContentBarStr = '<b>Weather Forecast</b>: ';
    for(var i = 0; i < obj.dailyForecasts.forecastLocation.forecast.length; i++) {
        var forecastDay = obj.dailyForecasts.forecastLocation.forecast[i];
        var hi = Math.round(convertFromCelciusToFarenheit(Number(forecastDay.highTemperature)));
        var lo = Math.round(convertFromCelciusToFarenheit(Number(forecastDay.lowTemperature)));
        //console.log('i' + i + ': ' + forecastDay.weekday + ', Hi: ' + hi + ', Lo: ' + lo + ', Precip: ' + forecastDay.precipitationProbability + '%, Wind: ' + forecastDay.windSpeed + ' ' + forecastDay.windDescShort);
        htmlStr += 
            // Card container div
            '<div class=\'card forecast-card\'>' +
            // Another ard container div
            '<div class=\'card-body text-center\'>' +
            // Weekday 
            '<h5 class=\'card-title forecast-day\'>' + forecastDay.weekday + '</h5>' +
            // Weather icon/image
            '<img src=\'' + determineWeatherIcon(forecastDay.iconLink, forecastDay.iconName) + '\'>' +
            // High temperature for the day
            '<h5 class=\'card-subtitle forecast-temp-hi\'>' + hi + '&deg;F' + '</h5>' +
            // Low temperature for the day
            '<h5 class=\'card-subtitle forecast-temp-lo\'>' + lo + '&deg;F' + '</h5>' +
            // Description of the weather for the day
            '<h6 class=\'card-subtitle forecast-weather\'>' + capitalizeDescription(forecastDay.description) + '</h6>' +
            // Paragraph containing information about precipitation and wind speed
            '<p class=\'card-text forecast-info\'>' +
            'Precip: ' + forecastDay.precipitationProbability + '%<br>' +
            'Wind: ' + Math.round(Number(forecastDay.windSpeed)) + ' MPH ' + forecastDay.windDescShort +
            '</p></div></div>'; 
        // Content bar formatting
        htmlContentBarStr +=
            // Weekday
            '<b>' + forecastDay.weekday + '</b> - ' + 
            // High and low temperature for the day
            hi + '&deg;F' + ' / ' +
            lo + '&deg;F' +
            // Precipitation for the day
            ', ' + forecastDay.precipitationProbability + '% Precip';
        // Only add the vertical bar if its not the last piece of data
        if(i != (obj.dailyForecasts.forecastLocation.forecast.length - 1)) {
            htmlContentBarStr += ' | ';
        }
    }
    document.getElementById(EL_WEATHER_FULL_CONTENT).innerHTML = htmlStr;
    document.getElementById(EL_WEATHER_INFOBAR_CONTENT).innerHTML = htmlContentBarStr;
    
    var forecastWeatherDescs = document.getElementsByClassName('forecast-weather');
    //document.getElementById('fs-pg-1').style.display = 'block';
    for(var i = 0; i < forecastWeatherDescs.length; i++) {
        console.log('fw ' + i + ': ' + forecastWeatherDescs[i].scrollWidth);
        if(forecastWeatherDescs[i].offsetWidth < forecastWeatherDescs[i].scrollWidth) {
            forecastWeatherDescs[i].style.animation = 'weatherDescScroll 30s linear infinite';
            forecastWeatherDescs[i].style.width = forecastWeatherDescs[i].scrollWidth + 'px';
        }
    }
    //document.getElementById('fs-pg-1').style.display = 'none';
    
}

function determineWeatherIcon(iconLink, iconName) {
    switch(iconName) {
        case "cw_no_report_icon":                   // 0
            return 'icons/er.png';
        case "sunny":                               // 1
            return 'icons/01d.png';
        case "clear":                               // 2
            return 'icons/01d.png';
        case "night_clear":                         // 3
            return 'icons/01n.png';
        case "mostly_sunny":                        // 4
        case "more_sun_than_clouds":
            return 'icons/02d.png';
        case "mostly_clear":                        // 5
        case "passing_clouds":
        case "scattered_clouds":
        case "clearing_skies":
        case "decreasing_cloudiness":
        case "broken_clouds":
            return 'icons/02d.png';
        case "night_mostly_clear":                  // 6
        case "night_passing_clouds":
        case "night_scattered_clouds":
        case "night_clearing_skies":
        case "night_decreasing_cloudiness":
        case "night_broken_clouds":
            return 'icons/02n.png';
        case "partly_sunny":                        // 7
        case "a_mixture_of_sun_and_clouds":
            return 'icons/04d.png';
        case "partly_cloudy":                       // 8
        case "increasing_cloudiness":
        case "more_clouds_than_sun":
            return 'icons/04d.png';
        case "breaks_of_sun_late":                  // 9
            return 'icons/04d.png';
        case "night_partly_cloudy":                 // 10
            return 'icons/04d.png';
        case "morning_clouds":                      // 11
            return 'icons/03d.png';
        case "afternoon_clouds":                    // 12
            return 'icons/03d.png';
        case "mostly_cloudy":                       // 13
            return 'icons/03d.png';
        case "night_morning_clouds":                // 14
            return 'icons/03d.png';
        case "night_afternoon_clouds":              // 15
            return 'icons/03d.png';
        case "night_mostly_cloudy":                 // 16
            return 'icons/03d.png'; 
        case "cloudy":                              // 17
        case "overcast":
            return 'icons/03d.png';
        case "low_clouds":                          // 18
            return 'icons/03d.png';
        case "high_level_clouds":                   // 19
        case "high_clouds":
            return 'icons/03d.png';
        case "night_high_level_clouds":             // 20
        case "night_high_clouds":
            return 'icons/03d.png';
        case "sprinkles_early":                     // 21
        case "light_rain_early":
            return 'icons/10d.png';
        case "light_rain":                          // 22
        case "drizzle":
        case "sprinkles":
        case "light_showers":
        case "a_few_showers":
        case "passing_showers":
        case "scattered_showers":
            return 'icons/10d.png';
        case "sprinkles_late":                      // 23
        case "light_rain_late":
            return 'icons/10d.png';
        case "night_sprinkles":                     // 24
        case "night_light_showers":
        case "night_a_few_showers":
        case "night_passing_showers":
        case "night_scattered_showers":
            return 'icons/10n.png';
        case "showers_early":                       // 25
        case "rain_early":
            return 'icons/09d.png';
        case "showerly":                            // 26
        case "showers":
        case "rain":
        case "rain_showers":
            return 'icons/09d.png';
        case "showers_late":                        // 27
        case "rain_late":
            return 'icons/09d.png';
        case "night_showers":                       // 28
        case "night_rain_showers":
            return 'icons/09d.png';
        case "heavy_rain_early":                    // 29
            return 'icons/09d.png';
        case "numerous_showers":                    // 30
        case "heavy_rain":
        case "lots_of_rain":
        case "tons_of_rain":
            return 'icons/09d.png';
        case "heavy_rain_late":                     // 31
            return 'icons/09d.png';
        case "flash_floods":                        // 32
        case "flood":
            return 'icons/09d.png';
        case "a_few_tstorms":                       // 33
        case "widely_scattered_tstorms":
        case "isolated_tstorms":
        case "scattered_tstorms":
            return 'icons/11d.png';
        case "scattered_tstorms_late":              // 34
        case "isolated_tstorms_late":
            return 'icons/11d.png';
        case "night_a_few_tstorms":                 // 35
        case "night_widely_scattered_tstorms":
        case "night_isolated_tstorms":
        case "night_scattered_tstorms":
            return 'icons/11d.png';
        case "tstorms_early":                       // 36
            return 'icons/11d.png';
        case "thundershowers":                      // 37
        case "tstorms":
        case "thunderstorms":
            return 'icons/11d.png';
        case "tstorms_late":                        // 38
            return 'icons/11d.png';
        case "night_tstorms":                       // 39
            return 'icons/11d.png';
        case "hail":                                // 40
        case "strong_thunderstorms":
            return 'icons/11d.png';
        case "severe_thunderstorms":                // 41
        case "tornado":
            return 'icons/11d.png';
        case "light_freezing_rain":                 // 42
        case "freezing_rain":
            return 'icons/13d.png';
        case "light_icy_mix_early":                 // 43
            return 'icons/13d.png';
        case "light_mixture_of_precip":             // 44
            return 'icons/13d.png';
        case "light_icy_mix_late":                  // 45
            return 'icons/13d.png';
        case "icy_mix_early":                       // 46
            return 'icons/13d.png';
        case "an_icy_mix_changing_to_rain":         // 47
        case "rain_changing_to_an_icy_mix":
        case "snow_changing_to_rain":
        case "sleet":
        case "mixture_of_precip":
        case "icy_mix":
        case "snow_rain_mix":
        case "snow_changing_to_an_icy_mix":
        case "rain_changing_to_snow":
        case "an_icy_mix_changing_to_snow":
        case "heavy_mixture_of_precip":
            return 'icons/13d.png';
        case "icy_mix_late":                        // 48
            return 'icons/13d.png';
        case "light_snow_early":                    // 49
            return 'icons/13d.png';
        case "light_snow_showers":                  // 50
        case "light_snow":
            return 'icons/13d.png';
        case "light_snow_late":                     // 51
            return 'icons/13d.png'; 
        case "heavy_snow_early":                    // 52
        case "flurries_early":
        case "snow_showers_early":
        case "snow_early":
            return 'icons/13d.png';
        case "scattered_flurries":                  // 53
        case "snow_flurries":
        case "snow_showers":
        case "snow":
        case "moderate_snow":
        case "heavy_snow":
            return 'icons/13d.png';
        case "flurries_late":                       // 54
        case "snow_showers_late":
        case "snow_late":
        case "heavy_snow_late":
            return 'icons/13d.png';
        case "snowstorm":                           // 55
        case "blizzard":
            return 'icons/13d.png';
        case "light_fog":                           // 56
            return 'icons/50d.png';
        case "early_fog_followed_by_sunny_skies":   // 57
            return 'icons/50d.png';
        case "early_fog":                           // 58
            return 'icons/50d.png';
        case "fog":                                 // 59
            return 'icons/50d.png';
        case "dense_fog":                           // 60
            return 'icons/50d.png';
        case "hazy_sunshine":                       // 61
            return 'icons/50d.png';
        case "haze":                                // 62
        case "smoke":
            return 'icons/50d.png';
        case "low_level_haze":                      // 63
            return 'icons/50d.png';
        case "night_haze":                          // 64
        case "night_smoke":
            return 'icons/50n.png';
        case "night_low_level_haze":                // 65
            return 'icons/50n.png';
        case "ice_fog":                             // 66
            return 'icons/50d.png';
        case "tropical_storm":                      // 67
        case "hurricane":
            return 'icons/er.png';
        case "sandstorm":                           // 68
        case "duststorm":
            return 'icons/er.png';
        default:
            return 'icons/er.png';
    }
}

/*
function determineWeatherBackground(iconDesc) {
    var backgroundInfoCat = determineWeatherCategory(iconDesc);
    var backgroundSet = weatherBackgrounds[backgroundInfoCat];
    if(backgroundSet.length == 0) {
        backgroundSet = weatherBackgrounds[0];
    }
    var backgroundInfo = backgroundSet[Math.floor(Math.random() * backgroundSet.length)].split('|');
    
    document.getElementById('foreground').style.backgroundImage = 
        'linear-gradient(rgba' + backgroundInfo[1] + ' 10%, rgba ' + backgroundInfo[2] + ', rgba' + backgroundInfo[3] + ' 100%)';
    document.getElementById('background').style.backgroundImage = 'url(' + backgroundInfo[0] + ')';
}
*/
/**
	Determine weekday of the week along with the time given a date object.
	
	**Should convert into a utility function for "/wslib/util/util.js"
 */
function determineDay(date) {
    var weekDay = date.getDay();
    var weekDayStr;
    switch(weekDay) {
        case 0:
            weekDayStr = 'Sunday';
            break;
        case 1:
            weekDayStr = 'Monday';
            break;
        case 2:
            weekDayStr = 'Tuesday';
            break;
        case 3:
            weekDayStr = 'Wednesday';
            break;
        case 4:
            weekDayStr = 'Thursday';
            break;
        case 5:
            weekDayStr = 'Friday';
            break;
        case 6:
            weekDayStr = 'Saturday';
            break;
        default:
            break;
    }
    var hours = date.getHours();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
	// I don't think this line below actually does anything...
    hours = hours ? hours : 12; // the hour '0' should be '12'
    return weekDayStr + ', ' + hours + ' ' + ampm;
}

/**
	Formats the given precipitation data (a converted JSON object) and returns a formatted string 
	  with units.
 */
function determinePrecipitation(data) {
	// Data from OpenWeatherMap API for this part CAN BE EMPTY OR NULL
	//   because it is not written.
    if(data != null && data['3h'] != null) {
        return data['3h'].toFixed(2) + ' mm';
    } else {
        return '0 mm';
    }
}

/**
	Captializes the the words of a given input string (data). 
	
	**Should relocate to "/wslib/util/util.js"
 */
function capitalizeDescription(data) {
    if(data != null) {
        var splitArr = data.split(" ");
        var returnString = '';
        for(var i = 0; i < splitArr.length; i++) {
            returnString += splitArr[i].charAt(0).toUpperCase() + splitArr[i].substr(1) + ' ';
        }
        return returnString;
    } else {
        return 'No Data';
    }
}

function convertFromCelciusToFarenheit(num) {
    return (num*(9/5)+32);
}

function determineWeatherCategory(iconDesc) {
    switch(iconDesc) {
        case "cw_no_report_icon":                   // 0
            return 0;
        case "sunny":                               // 1
            return 1;
        case "clear":                               // 2
            return 2;
        case "night_clear":                         // 3
            return 3;
        case "mostly_sunny":                        // 4
        case "more_sun_than_clouds":
            return 4;
        case "mostly_clear":                        // 5
        case "passing_clouds":
        case "scattered_clouds":
        case "clearing_skies":
        case "decreasing_cloudiness":
        case "broken_clouds":
            return 5;
        case "night_mostly_clear":                  // 6
        case "night_passing_clouds":
        case "night_scattered_clouds":
        case "night_clearing_skies":
        case "night_decreasing_cloudiness":
        case "night_broken_clouds":
            return 6;
        case "partly_sunny":                        // 7
        case "a_mixture_of_sun_and_clouds":
            return 7;
        case "partly_cloudy":                       // 8
        case "increasing_cloudiness":
        case "more_clouds_than_sun":
            return 8;
        case "breaks_of_sun_late":                  // 9
            return 9;
        case "night_partly_cloudy":                 // 10
            return 10;
        case "morning_clouds":                      // 11
            return 11;
        case "afternoon_clouds":                    // 12
            return 12;
        case "mostly_cloudy":                       // 13
            return 13;
        case "night_morning_clouds":                // 14
            return 14;
        case "night_afternoon_clouds":              // 15
            return 15;
        case "night_mostly_cloudy":                 // 16
            return 16; 
        case "cloudy":                              // 17
        case "overcast":
            return 17;
        case "low_clouds":                          // 18
            return 18;
        case "high_level_clouds":                   // 19
        case "high_clouds":
            return 19;
        case "night_high_level_clouds":             // 20
        case "night_high_clouds":
            return 20;
        case "sprinkles_early":                     // 21
        case "light_rain_early":
            return 21;
        case "light_rain":                          // 22
        case "drizzle":
        case "sprinkles":
        case "light_showers":
        case "a_few_showers":
        case "passing_showers":
        case "scattered_showers":
            return 22;
        case "sprinkles_late":                      // 23
        case "light_rain_late":
            return 23;
        case "night_sprinkles":                     // 24
        case "night_light_showers":
        case "night_a_few_showers":
        case "night_passing_showers":
        case "night_scattered_showers":
            return 24;
        case "showers_early":                       // 25
        case "rain_early":
            return 25;
        case "showerly":                            // 26
        case "showers":
        case "rain":
        case "rain_showers":
            return 26;
        case "showers_late":                        // 27
        case "rain_late":
            return 27;
        case "night_showers":                       // 28
        case "night_rain_showers":
            return 28;
        case "heavy_rain_early":                    // 29
            return 29;
        case "numerous_showers":                    // 30
        case "heavy_rain":
        case "lots_of_rain":
        case "tons_of_rain":
            return 30;
        case "heavy_rain_late":                     // 31
            return 31;
        case "flash_floods":                        // 32
        case "flood":
            return 32;
        case "a_few_tstorms":                       // 33
        case "widely_scattered_tstorms":
        case "isolated_tstorms":
        case "scattered_tstorms":
            return 33;
        case "scattered_tstorms_late":              // 34
        case "isolated_tstorms_late":
            return 34;
        case "night_a_few_tstorms":                 // 35
        case "night_widely_scattered_tstorms":
        case "night_isolated_tstorms":
        case "night_scattered_tstorms":
            return 35;
        case "tstorms_early":                       // 36
            return 36;
        case "thundershowers":                      // 37
        case "tstorms":
        case "thunderstorms":
            return 37;
        case "tstorms_late":                        // 38
            return 38;
        case "night_tstorms":                       // 39
            return 39;
        case "hail":                                // 40
        case "strong_thunderstorms":
            return 40;
        case "severe_thunderstorms":                // 41
        case "tornado":
            return 41;
        case "light_freezing_rain":                 // 42
        case "freezing_rain":
            return 42;
        case "light_icy_mix_early":                 // 43
            return 43;
        case "light_mixture_of_precip":             // 44
            return 44;
        case "light_icy_mix_late":                  // 45
            return 45;
        case "icy_mix_early":                       // 46
            return 46;
        case "an_icy_mix_changing_to_rain":         // 47
        case "rain_changing_to_an_icy_mix":
        case "snow_changing_to_rain":
        case "sleet":
        case "mixture_of_precip":
        case "icy_mix":
        case "snow_rain_mix":
        case "snow_changing_to_an_icy_mix":
        case "rain_changing_to_snow":
        case "an_icy_mix_changing_to_snow":
        case "heavy_mixture_of_precip":
            return 47;
        case "icy_mix_late":                        // 48
            return 48;
        case "light_snow_early":                    // 49
            return 49;
        case "light_snow_showers":                  // 50
        case "light_snow":
            return 50;
        case "light_snow_late":                     // 51
            return 51; 
        case "heavy_snow_early":                    // 52
        case "flurries_early":
        case "snow_showers_early":
        case "snow_early":
            return 52;
        case "scattered_flurries":                  // 53
        case "snow_flurries":
        case "snow_showers":
        case "snow":
        case "moderate_snow":
        case "heavy_snow":
            return 53;
        case "flurries_late":                       // 54
        case "snow_showers_late":
        case "snow_late":
        case "heavy_snow_late":
            return 54;
        case "snowstorm":                           // 55
        case "blizzard":
            return 55;
        case "light_fog":                           // 56
            return 56;
        case "early_fog_followed_by_sunny_skies":   // 57
            return 57;
        case "early_fog":                           // 58
            return 58;
        case "fog":                                 // 59
            return 59;
        case "dense_fog":                           // 60
            return 60;
        case "hazy_sunshine":                       // 61
            return 61;
        case "haze":                                // 62
        case "smoke":
            return 62;
        case "low_level_haze":                      // 63
            return 63;
        case "night_haze":                          // 64
        case "night_smoke":
            return 64;
        case "night_low_level_haze":                // 65
            return 65;
        case "ice_fog":                             // 66
            return 66;
        case "tropical_storm":                      // 67
        case "hurricane":
            return 67;
        case "sandstorm":                           // 68
        case "duststorm":
            return 68;
        default:
            return 0;
    }
}